# Cloudflare.md

This is a tutorial on how to get free CDN service from Cloudflare

## Регистрация

Посетите [cloudflare.com](https://cloudflare.com) и заведите там аккаунт.
Зайдите на сайт под своим именем, верифицируйте почтовый адрес.

## Добавьте сайт

Нажмите +Add site и введите имя домена

![cf-addsite-1.png](img/cf-addsite-1.png)

Выберите бесплатный план и нажмите Confirm Plan

![cf-select-plan-1.png](img/cf-select-plan-1.png)

Cloudflare запросит DNS записи вашего домена и покажет вам для проверки:

![cf-review-records.png](img/cf-review-records.png)

Если все правильно, жмите Continue.

На следующей странице будут представлены инструкции по замене DNS nameserver вашего текущего провайдера на сервера Cloudflare.

## Замена DNS nameservers

В нашем случае, записи DNS хранятся в DNS namserver нашего регистратора Freenom.

Зайдите на сайт [freenom.com](https://freenom.com) под вашим логином и перейдите в секцию [My domains](https://my.freenom.com/clientarea.php?action=domains).

![fn-my-domains.png](img/fn-my-domains.png)

Нажмите Manage Domain напротив домена, для которого хотите изменить DNS.

На страничке управления доменом выберите NAmeservers в меню Management Tools.

![fn-manage-nameservers.png](img/fn-manage-nameservers.png)

Переключите на Use custom nameservers, введите имена серверов Cloudflare и нажмите Change Nameservers.

![fn-change-ns.png](img/fn-change-ns.png)

Теперь остается ждать смены серверов. Проследить за ней можно используя команду `host`:

![cf-host-ns.png](img/cf-host-ns.png)

Теперь вернитесь в дашбоард Cloudflare и нажмите "Done, check now" или "Re-check now".

Cloudflare известит вас письмом, когда ваш сайт активируют.

## Работа с DNS записями в Cloudflare

Если надо сменить статический аддрес вашего сайта, например, при переходе на другое облако, то задите во вкладку DNS и отредактируйте соответствующий рекорд

![cf-dns-change-A.png](img/cf-dns-change-A.png)

## Проверяем 

После того, Cloudflare переведет ваш сайт на CDN, доменное имя будет разрешаться по адресу, принадлежащему Cloudflare, хотя ваш сайт и будет доступен напрямую по его ip-адресу хостинг-провайдера.

Проверить можно командой host:

![cf-host-ip-dns.png](img/cf-host-ip-dns.png)

Командой whois мы получили сведения об адресе - он принадлежит сетям Cloudfalre.

Cloudflare так же обещает, что даже если ваш сайт недоступен из-за проблем вашего хостинг-провайдера или проблем с самим сайтом, то CDN будет продолжать выдавать контент из своего кэша. Убедитесь в этом сами!


## Links

[Step by step instructions](https://support.cloudflare.com/hc/articles/205195708)